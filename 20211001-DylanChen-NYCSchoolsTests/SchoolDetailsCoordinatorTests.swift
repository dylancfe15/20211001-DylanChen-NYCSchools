//
//  SchoolDetailsCoordinatorTests.swift
//  20211001-DylanChen-NYCSchoolsTests
//
//  Created by Difeng Chen on 10/3/21.
//

import XCTest
@testable import _0211001_DylanChen_NYCSchools

class SchoolDetailsCoordinatorTests: XCTestCase {

    private let coordinator = SchoolDetailsCoordinator()

    private func testSAT() {
        XCTAssertNil(coordinator.sat)

        coordinator.sat = SAT(sat_critical_reading_avg_score: nil, sat_math_avg_score: nil, sat_writing_avg_score: nil)

        XCTAssertNotNil(coordinator.sat)
    }
}
