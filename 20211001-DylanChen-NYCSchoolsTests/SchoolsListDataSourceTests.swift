//
//  SchoolsListDataSourceTests.swift
//  20211001-DylanChen-NYCSchoolsTests
//
//  Created by Difeng Chen on 10/3/21.
//

import XCTest
@testable import _0211001_DylanChen_NYCSchools

class SchoolsListDataSourceTests: XCTestCase {

    private let dataSource = SchoolsListDataSource(tableView: SchoolsListTableView())

    private func testReload() {
        XCTAssert(dataSource.schools.isEmpty)

        let schools = [
            School(dbn: nil, school_name: nil, overview_paragraph: nil, school_email: nil, phone_number: nil, website: nil, latitude: nil, longitude: nil, total_students: nil, location: nil),
            School(dbn: nil, school_name: nil, overview_paragraph: nil, school_email: nil, phone_number: nil, website: nil, latitude: nil, longitude: nil, total_students: nil, location: nil),
        ]

        dataSource.reload(with: schools)

        XCTAssertEqual(dataSource.schools.count, 2)
    }
}
