//
//  School.swift
//  20211001-DylanChen-NYCSchools
//
//  Created by Difeng Chen on 10/2/21.
//

import Foundation

struct School: Decodable {

    // MARK: - Properties
    
    let dbn: String?
    let school_name: String?
    let overview_paragraph: String?
    let school_email: String?
    let phone_number: String?
    let website: String?
    let latitude: String?
    let longitude: String?
    let total_students: String?
    let location: String?
}
