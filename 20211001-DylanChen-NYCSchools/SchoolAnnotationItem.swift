//
//  SchoolAnnotationItem.swift
//  20211001-DylanChen-NYCSchools
//
//  Created by Difeng Chen on 10/2/21.
//

import Foundation
import CoreLocation

struct SchoolAnnotationItem: Identifiable {
    
    // MARK: - Properties

    let id: String
    let coordinate: CLLocationCoordinate2D
}
