//
//  SectionTitleText.swift
//  20211001-DylanChen-NYCSchools
//
//  Created by Difeng Chen on 10/2/21.
//

import SwiftUI

struct SectionTitleText: View {

    // MARK: -  Properties

    let text: String

    // MARK: - Initializers

    init(_ text: String) {
        self.text = text
    }

    // MARK: - Views
    var body: some View {
        Text(text)
            .font(.title)
            .fontWeight(.bold)
    }
}
