//
//  SchoolListViewController.swift
//  20211001-DylanChen-NYCSchools
//
//  Created by Difeng Chen on 10/1/21.
//

import UIKit

class SchoolsListViewController: UIViewController {

    // MARK: - Properties

    private lazy var coordinator = SchoolsListCoodinator(controller: self)

    // MARK: - Functions

    override func viewDidLoad() {
        super.viewDidLoad()

        coordinator.configure()
    }
}
