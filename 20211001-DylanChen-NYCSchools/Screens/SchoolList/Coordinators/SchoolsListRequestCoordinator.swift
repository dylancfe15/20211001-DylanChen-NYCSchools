//
//  SchoolListRequestCoordinator.swift
//  20211001-DylanChen-NYCSchools
//
//  Created by Difeng Chen on 10/2/21.
//

import Foundation

class SchoolsListRequestCoordinator {

    // MARK: - Functions

    /// Fetch a list of schools from the server
    /// - Parameter completion: returns a list of schools if available
    func getSchools(completion: @escaping(_ schools: [School]?, _ succeed: Bool) -> Void) {
        // TODO: - Refactor URLs to configuration
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else {
            return
        }

        var request = URLRequest(url: url)

        request.httpMethod = "GET"

        // FIXME: - Ideally, we should apply pagination here, but the API doesn't seem to support it.
        let task = URLSession.shared.dataTask(with: request) { data, _, _ in
            guard let data = data,
                    let schools = try? JSONDecoder().decode([School].self, from: data) else {
                completion(nil, false)
                return
            }

            completion(schools, true)
        }

        task.resume()
    }
}
