//
//  SchoolListViewCoordinator.swift
//  20211001-DylanChen-NYCSchools
//
//  Created by Difeng Chen on 10/2/21.
//

import UIKit

protocol SchoolsListViewDelegate: AnyObject {

    func didScroll(offset: CGSize)
}

class SchoolsListViewCoordinator {

    // MARK: - Properties

    let containerView: UIView

    private let coordinator: SchoolsListCoodinator
    private let stackView = SchoolsListContainerStackView()
    private let logoImageView = LogoImageView(frame: .zero)
    private let listTableView = SchoolsListTableView(frame: .zero, style: .plain)

    // MARK: - Initializers

    init(coordinator: SchoolsListCoodinator) {
        self.coordinator = coordinator
        self.containerView = coordinator.controller.view
    }

    // MARK: - Functions

    /// Configure views
    func configure() {
        containerView.addSubview(stackView)

        stackView.addArrangedSubview(logoImageView)
        stackView.addArrangedSubview(listTableView)

        listTableView.schoolsListDelegate = coordinator
    }


    /// Reload view with the provided data
    /// - Parameter schools: a list of school objects
    func reload(with schools: [School]) {
        listTableView.reload(with: schools)
    }
}
