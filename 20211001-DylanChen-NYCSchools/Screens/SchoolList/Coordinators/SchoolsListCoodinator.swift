//
//  SchoolListCoordinator.swift
//  20211001-DylanChen-NYCSchools
//
//  Created by Difeng Chen on 10/2/21.
//

import SwiftUI

class SchoolsListCoodinator {

    // MARK: - Properties

    let controller: SchoolsListViewController
    let requestCoordinator = SchoolsListRequestCoordinator()

    private lazy var viewCoordinator = SchoolsListViewCoordinator(coordinator: self)

    // MARK: - Initializers

    init(controller: SchoolsListViewController) {
        self.controller = controller
    }

    // MARK: - Functions

    /// Configure data and views
    func configure() {
        viewCoordinator.configure()

        configureRequests()
    }

    /// Configure http requests
    private func configureRequests() {
        requestCoordinator.getSchools { [weak self] schools, succeed in
            if succeed, let schools = schools {
                self?.viewCoordinator.reload(with: schools)
            } else {
                // Error handler
                let alert = UIAlertController(title: "Ops...", message: "Something went wrong when fetching data from the server, please try again later.", preferredStyle: .alert)

                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "Try Again", style: .default, handler: { _ in
                    self?.configureRequests()
                }))

                self?.controller.present(alert, animated: true)
            }
        }
    }
}

// MARK: - SchoolsListDelegate

extension SchoolsListCoodinator: SchoolsListDelegate {

    func didSelect(school: School) {
        // Demostrating my SwiftUI skills
        let detailsView = UIHostingController(rootView: SchoolDetailsView(school: school))

        controller.present(detailsView, animated: true, completion: nil)
    }
}
