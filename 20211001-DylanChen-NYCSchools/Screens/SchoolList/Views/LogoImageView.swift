//
//  LogoImageView.swift
//  20211001-DylanChen-NYCSchools
//
//  Created by Difeng Chen on 10/3/21.
//

import UIKit

class LogoImageView: UIImageView {

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)

        image = UIImage(named: "logo")
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: -  Functions

    override func didMoveToSuperview() {
        super.didMoveToSuperview()

        translatesAutoresizingMaskIntoConstraints = false

        setConstraints(width: 250, height: 50)
    }
}
