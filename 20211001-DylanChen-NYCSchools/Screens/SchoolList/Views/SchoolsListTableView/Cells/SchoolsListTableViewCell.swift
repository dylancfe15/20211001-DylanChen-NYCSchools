//
//  SchoolsListTableViewCell.swift
//  20211001-DylanChen-NYCSchools
//
//  Created by Difeng Chen on 10/2/21.
//

import UIKit

class SchoolsListTableViewCell: UITableViewCell {

    // MARK: - Views

    private let stackView: UIStackView = {
        let stackView = UIStackView()

        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 5
        stackView.alignment = .leading

        return stackView
    }()

    private let nameLable: UILabel = {
        let label = UILabel()

        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 25, weight: .heavy)

        return label
    }()

    private let numberOfStudentsLabel: UILabel = {
        let label = UILabel()

        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = .gray

        return label
    }()

    private let bottomSeparator: UIView = {
        let view = UIView()

        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray

        return view
    }()

    // MARK: - Properties

    private var school: School? {
        didSet {
            reloadUI()
        }
    }

    static var identifier: String {
        String(describing: type(of: self))
    }

    // MARK: - Initializers

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        selectionStyle = .none

        addSubview(stackView)

        stackView.setConstraints(leading: 0, top: 10, trailing: 0, bottom: -10)

        stackView.addArrangedSubview(nameLable)
        stackView.addArrangedSubview(numberOfStudentsLabel)
        stackView.addArrangedSubview(bottomSeparator)

        nameLable.setConstraints(leading: 10, trailing: -10)

        bottomSeparator.setConstraints(height: 1)
        bottomSeparator.setConstraints(leading: 20, trailing: -20)

        layoutIfNeeded()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: - Functions

    /// Configure the data for the cell
    /// - Parameter school: a school object to configure
    func configure(school: School) {
        self.school = school
    }


    /// Reload the UI to reflace the up-to-date data
    private func reloadUI() {
        nameLable.text = school?.school_name

        if let numberOfStudents = school?.total_students {
            numberOfStudentsLabel.text = "Number of Students: \(numberOfStudents)"
        }
    }
}
