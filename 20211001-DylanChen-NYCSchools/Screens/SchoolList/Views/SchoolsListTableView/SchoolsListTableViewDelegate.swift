//
//  SchoolsListTableViewDelegate.swift
//  20211001-DylanChen-NYCSchools
//
//  Created by Difeng Chen on 10/2/21.
//

import UIKit

class SchoolsListTableViewDelegate: NSObject, UITableViewDelegate {

    // MARK: -  Properties

    private var dataSource: SchoolsListDataSource?

    // MARK: - Initializers

    convenience init(dataSource: SchoolsListDataSource) {
        self.init()

        self.dataSource = dataSource
    }

    // MARK: - UITableViewDelegate Functions

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let tableView = tableView as? SchoolsListTableView,
            let dataSource = dataSource else {
            return
        }

        let school = dataSource.schools[indexPath.row]

        tableView.schoolsListDelegate?.didSelect(school: school)
    }
}
