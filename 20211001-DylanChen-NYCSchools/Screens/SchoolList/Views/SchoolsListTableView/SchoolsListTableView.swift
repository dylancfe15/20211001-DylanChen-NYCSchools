//
//  SchoolsListTableView.swift
//  20211001-DylanChen-NYCSchools
//
//  Created by Difeng Chen on 10/2/21.
//

import UIKit

protocol SchoolsListDelegate: AnyObject {

    func didSelect(school: School)
}

class SchoolsListTableView: UITableView {

    // MARK: - Properties

    private lazy var tableViewDataSource = SchoolsListDataSource(tableView: self)
    private lazy var tableViewDelegate = SchoolsListTableViewDelegate(dataSource: tableViewDataSource)

    weak var schoolsListDelegate: SchoolsListDelegate?

    // MARK: - Initializers

    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)

        dataSource = tableViewDataSource
        delegate = tableViewDelegate

        separatorStyle = .none
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: - Functions

    override func didMoveToSuperview() {
        super.didMoveToSuperview()

        translatesAutoresizingMaskIntoConstraints = false

        setConstraints(leading: 0, trailing: 0)
    }

    /// reload tableView with the given list of schools
    /// - Parameter schools: a list of schools to reload
    func reload(with schools: [School]) {
        tableViewDataSource.reload(with: schools)
    }
}
