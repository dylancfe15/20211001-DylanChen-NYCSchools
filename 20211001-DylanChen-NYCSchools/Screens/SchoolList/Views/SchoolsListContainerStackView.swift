//
//  SchoolsListContainerStackView.swift
//  20211001-DylanChen-NYCSchools
//
//  Created by Difeng Chen on 10/2/21.
//

import UIKit

class SchoolsListContainerStackView: UIStackView {

    // MARK: -  Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)

        axis = .vertical
        alignment = .center
        spacing = 8
    }

    required init(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: - Functions

    override func didMoveToSuperview() {
        super.didMoveToSuperview()

        translatesAutoresizingMaskIntoConstraints = false

        setConstraints(leading: 0, top: 44, trailing: 0, bottom: 0)
    }
}
